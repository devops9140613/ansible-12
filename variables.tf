variable "do_token" {
  type = string
}

variable "sshkey" {
  type = string
}

variable "accesskey"{
  type = string
}

variable "images"{
  type = list(string)
  default = ["ubuntu-20-04-x64", "ubuntu-20-04-x64"]
}

variable "secretkey"{
  type = string
}

variable "pass" {
  type = string
}

variable "devs" {
  type = list(string)
  default =  ["app1-imslttan-at-yandex-ru", "lb-imslttan-at-yandex-ru"]
  }


variable "mail" {
  type = string
}

variable "task" {
  type = string
}
