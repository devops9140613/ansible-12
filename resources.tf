data "digitalocean_ssh_key" "rebrain_sshkey" {
  name = "REBRAIN.SSH.PUB.KEY"
}

resource "random_string" "password" {
  count = length(var.devs)
  length = "14"
  upper = true
  lower = true
  numeric = true
  special = false  
}

resource "digitalocean_droplet" "VPS" {
  count = length(var.devs)
  tags = [var.task, var.mail]
  image = var.images[count.index]
  region = "fra1"
  name = var.devs[count.index]
  size = "s-1vcpu-1gb"
  ssh_keys =[data.digitalocean_ssh_key.rebrain_sshkey.id,var.sshkey]
connection {
  type = "ssh"
  user = "root"
  host = self.ipv4_address
  agent = true 
}
provisioner "remote-exec" {
   inline = [   
   "/bin/echo -e \"${element(random_string.password.*.result, count.index)}\n${element(random_string.password.*.result, count.index)}\" | /usr/bin/passwd root"
   ]
  }
}
resource "null_resource" "ansible" {
provisioner "local-exec" {
   command = "ansible-playbook -i inventory.yaml nginx.yaml"
  }
  depends_on = [local_file.logins]
}


data "aws_route53_zone" "test" {
  name = "devops.rebrain.srwx.net"
}

resource "aws_route53_record" "www" {
  count = length(var.devs)
  zone_id = data.aws_route53_zone.test.zone_id
  name    = "www.${var.devs[count.index]}"
  type    = "A"
  ttl     = "300"
  records = ["${element(digitalocean_droplet.VPS.*.ipv4_address,count.index)}"]
}

resource "local_file" "logins" {
  count = length(var.devs)
  content = templatefile ("./file.tmpl", 
  {
   webservers = {
names = aws_route53_record.www.*.name
ips = digitalocean_droplet.VPS.*.ipv4_address
pass = random_string.password.*.result
}
  })
  filename = "./inventory.yaml"
}

